--La suma de las cantidades e importe total de todas las entregas realizadas durante el 97.

SET DATEFORMAT DMY
SELECT SUM(e.cantidad) as 'Cantidad total', SUM(e.cantidad * (m.costo + (m.Costo * m.PorcentajeImpuesto))) as 'importe total'
FROM Materiales m, Entregan e
WHERE m.Clave = e.Clave
AND e.Fecha BETWEEN '01-01-1997' AND '31-12-1997'

--Para cada proveedor, obtener la raz�n social del proveedor,
--n�mero de entregas e importe total de las entregas realizadas

SELECT p.RFC, p.RazonSocial, COUNT(p.RFC) as 'Total de entregas', SUM(e.cantidad * (m.costo + (m.Costo * m.PorcentajeImpuesto))) as 'importe total'
FROM Proveedores p, Entregan e, Materiales m
WHERE p.RFC = e.RFC
AND m.Clave = e.Clave
GROUP BY p.RFC, p.RazonSocial

--Por cada material obtener la clave y descripci�n del material, la cantidad total entregada, la m�nima
--cantidad entregada, la m�xima cantidad entregada, el importe total de las entregas de aquellos materiales
--en los que la cantidad promedio entregada sea mayor a 400. 

SELECT m.Clave, m.Descripcion, SUM(e.Cantidad) as 'cantidad total entregada',
MIN(e.Cantidad) as 'M�nima cantidad entregada', MAX(e.Cantidad) as 'M�xima cantidad entregada',
AVG(e.Cantidad) as 'Promedio', SUM(e.cantidad * (m.costo + (m.Costo * m.PorcentajeImpuesto))) as 'importe total'
FROM Materiales m, Entregan e
WHERE m.Clave = e.Clave
GROUP BY m.Clave, m.Descripcion
HAVING AVG(e.Cantidad) > 400

--Para cada proveedor, indicar su raz�n social y mostrar la cantidad promedio de cada material entregado,
--detallando la clave y descripci�n del material, excluyendo aquellos proveedores para los que la cantidad
--promedio sea menor a 500

SELECT p.RazonSocial, m.Descripcion, m.Clave, AVG(e.Cantidad) as 'Cantidad Promedio'
FROM Proveedores p, Materiales m, Entregan e
WHERE p.RFC = e.RFC AND e.Clave = m.Clave
GROUP BY  p.RazonSocial, m.Descripcion, m.Clave
HAVING AVG(e.Cantidad) > 500
ORDER BY p.RazonSocial

--Mostrar en una solo consulta los mismos datos que en la consulta anterior pero para dos grupos de
--proveedores: aquellos para los que la cantidad promedio entregada es menor a 370 y aquellos para
--los que la cantidad promedio entregada sea mayor a 450.

SELECT p.RazonSocial, m.Descripcion, m.Clave, AVG(e.Cantidad) as 'Cantidad Promedio'
FROM Proveedores p, Materiales m, Entregan e
WHERE p.RFC = e.RFC AND e.Clave = m.Clave
GROUP BY  p.RazonSocial, m.Descripcion, m.Clave
HAVING AVG(e.Cantidad) < 370 OR AVG(e.Cantidad) > 450
ORDER BY p.RazonSocial

--Considerando que los valores de tipos CHAR y VARCHAR deben ir encerrados entre ap�strofes, los valores
--num�ricos se escriben directamente y los de fecha, como '1-JAN-00' para 1o. de enero del 2000, inserta cinco
--nuevos materiales

INSERT INTO Materiales VALUES (6661, 'Piedras del infierno', 666.00, 0) ; 
INSERT INTO Materiales VALUES (0420, 'KK', 420.00, 0) ; 
INSERT INTO Materiales VALUES (2112, 'Rush', 666.00, 0) ; 
INSERT INTO Materiales VALUES (1111, 'Piedra filosofal', 100000.00, .5) ; 

--Clave y descripci�n de los materiales que nunca han sido entregados. 

SELECT Clave, Descripcion
FROM Materiales m
WHERE m.Clave NOT IN(SELECT m.Clave
					FROM Materiales m, Entregan e
					WHERE m.Clave = e.Clave)

--Raz�n social de los proveedores que han realizado entregas tanto al proyecto 'Vamos M�xico' como al
--proyecto 'Quer�taro Limpio'. 

SELECT p.RazonSocial
FROM Proveedores p, Entregan e, Proyectos pr
WHERE p.RFC = e.RFC AND e.Numero = pr.Numero
AND pr.Denominacion = 'Vamos mexico'
AND p.RazonSocial IN (SELECT p.RazonSocial
		FROM Proveedores p, Entregan e, Proyectos pr
		WHERE p.RFC = e.RFC AND e.Numero = pr.Numero
		AND pr.Denominacion = 'Queretaro limpio')
GROUP BY p.RazonSocial

--Descripci�n de los materiales que nunca han sido entregados al proyecto 'CIT Yucat�n'.

SELECT m.Descripcion
FROM Materiales m
WHERE m.Clave NOT IN (SELECT m.Clave
						FROM Materiales m, Entregan e, Proyectos p
						WHERE m.Clave = e.Clave AND p.Numero = e.Numero
						AND p.Denominacion = 'CIT Yucatan')

--Raz�n social y promedio de cantidad entregada de los proveedores cuyo promedio de cantidad entregada
--es mayor al promedio de la cantidad entregada por el proveedor con el RFC 'VAGO780901'.

SELECT p.RFC, AVG(e.Cantidad) as 'Cantidad'
FROM Proveedores p, Entregan e
WHERE p.RFC = e.RFC
GROUP BY p.RFC
HAVING AVG(e.Cantidad) > (SELECT AVG(e.Cantidad)
							FROM Proveedores p, Entregan e
							WHERE p.RFC = e.RFC AND e.RFC = 'VAGO780901'
							GROUP BY p.RFC)

-- RFC, raz�n social de los proveedores que participaron en el proyecto 'Infonavit Durango' y cuyas cantidades
--totales entregadas en el 2000 fueron mayores a las cantidades totales entregadas en el 2001.

CREATE VIEW ENTREGAS_DURANGO2001(RFC, CantidadTotal)
AS
		SELECT e.RFC, SUM(e.Cantidad)
		FROM ENTREGAS_DURANGO1 ed, Entregan e
		WHERE ed.RFC = e.RFC 
		AND e.Fecha BETWEEN '01-01-2001' AND '31-12-2001'
		GROUP BY e.RFC

CREATE VIEW ENTREGAS_DURANGO2000(RFC, CantidadTotal)
AS
		SELECT e.RFC, SUM(e.Cantidad)
		FROM ENTREGAS_DURANGO1 ed, Entregan e
		WHERE ed.RFC = e.RFC 
		AND e.Fecha BETWEEN '01-01-2000' AND '31-12-2000'
		GROUP BY e.RFC

SELECT p.RazonSocial, p.RFC
FROM ENTREGAS_DURANGO2000 ed2000, ENTREGAS_DURANGO2001 ed2001, Proveedores p
WHERE ed2000.RFC = ed2001.RFC AND
ed2000.RFC = p.RFC
AND ed2000.CantidadTotal > ed2001.CantidadTotal