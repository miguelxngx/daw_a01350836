<?php

function _e($string) {
  return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
  //echo htmlentities($string, ENT_QUOTES, 'UTF-8');
}


if(isset($_POST["nombre"]) && isset($_POST["apellido"]) && isset($_POST["mail"]) && isset($_POST["con_mail"]) && isset($_POST["contrasenia"]) && isset($_POST["con_contrasenia"])){

    
    if(empty($_POST["nombre"]) || empty($_POST["apellido"]) || empty($_POST["mail"]) || empty($_POST["con_mail"]) || empty($_POST["contrasenia"]) || empty($_POST["con_contrasenia"])){
        $m = "Los datos que ingresaste no están completos";
        include("_Nav.html");
        include("_Error.html");
        include("_Footer.html");
    }
    else if($_POST["mail"] != $_POST["con_mail"]){
        $m = "Los correos que ingresaste no coinciden";
        include("_Nav.html");
        include("_Error.html");
        include("_Footer.html");
    }
    else if($_POST["contrasenia"] != $_POST["con_contrasenia"]){
        $m = "Las contraseñas que ingresaste no coinciden";
        include("_Nav.html");
        include("_Error.html");
        include("_Footer.html");
    }else{
        $fn=_e($_POST["nombre"]);
      $ln=_e($_POST["apellido"]);
      $em=_e($_POST["mail"]);
      $ps=_e($_POST["contrasenia"]);
      include("_Nav.html");
      include("_Valido.html");
      include("_Footer.html");
    }
}else{
    header("Location:index.php");
}


?>