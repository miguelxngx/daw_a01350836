<?php
    session_start();
    require_once("util.php");
    if(isset($_POST["id"])) {
        editarRegistro($_POST["id"],$_POST["name"], $_POST["units"], $_POST["quantity"], $_POST["price"], $_POST["country"]);
        $_SESSION["mensaje"] = $_POST["nombre"].' se actualizó correctamente';
    } else {
        /*guardarRegistro("naranja", 13, 45, 123, "España");*/
        guardarRegistro($_POST["name"], $_POST["units"], $_POST["quantity"], $_POST["precio"], $_POST["country"]);
        /*$_SESSION["mensaje"] = $_POST["nombre"].' se registró correctamente';*/
    }
    header("location:index.php");
?>