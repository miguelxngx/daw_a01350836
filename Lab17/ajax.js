function getRequestObject() {
  // Asynchronous objec created, handles browser DOM differences

  if(window.XMLHttpRequest) {
    // Mozilla, Opera, Safari, Chrome IE 7+
    return (new XMLHttpRequest());
  }
  else if (window.ActiveXObject) {
    // IE 6-
    return (new ActiveXObject("Microsoft.XMLHTTP"));
  } else {
    // Non AJAX browsers
    return(null);
  }
}


function getSuggestionsCountry(){
    request = getRequestObject();
    if(request != null){
        var input = document.getElementById('country');
        var url = 'paisesAjax.php?pattern='+input.value;
        request.open('GET', url, true);
        request.onreadystatechange = function() {
            if( request.readyState == 4){
                var ajaxResponse = document.getElementById('ajaxResponse');
                ajaxResponse.innerHTML = request.responseText;
                ajaxResponse.style.visibility = "visible";
            }
        }
        
        request.send(null);
    }
}

function selectValue(){
    console.log("selectValue");
    var list=document.getElementById("list");
    var userInput=document.getElementById("country");
    var ajaxResponse=document.getElementById('ajaxResponse');
    userInput.value=list.options[list.selectedIndex].text;
    ajaxResponse.style.visibility="hidden";
    userInput.focus();
}