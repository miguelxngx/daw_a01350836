<?php

session_start();

$target_dir = 'media/';
$target_file = $target_dir . basename($_FILES["foto"]["name"]);
$pa = basename($_FILES["foto"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image


if (isset($_FILES['foto']['error'])) {
        echo "Error code: ".$_FILES['foto']['error'];
}

if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["foto"]["tmp_name"]);
    if($check != false) {
        $uploadOk = 1;
    } else {
        echo "File is not an image."."<br>";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif"
&& $imageFileType != "JPG" && $imageFileType != "PNG" && $imageFileType !=  "JPEG" && $imageFileType != "GIF") {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}

$_SESSION["foto"] = $target_file;

include("_Nav.html");
include("_Content.html");
include("_Footer.html");

?>
