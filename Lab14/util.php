<?php

    function connectDb(){
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "dbname";
        
        $con = mysqli_connect($servername, $username, $password, $dbname);
        $con -> set_charset("utf8");
        
        //Check connection
        if(!$con){
            die("connection failed: " . mysqli_connect_error());
        }
        return $con;
    }

    function closeDb($mysql){
        mysqli_close($mysql);
    }

    function getFruits(){
        $conn = connectDb();
        $sql = "SELECT name, units, quantity, price, country FROM Fruit";
        $result = mysqli_query($conn, $sql);
        closeDb($conn);
        return $result;
    }

    function getFruitsCountry($country){
        $conn = connectDb();
        $sql = "SELECT name, units, quantity, price, country FROM Fruit WHERE country = '".$country."'";
        $result = mysqli_query($conn, $sql);
        if(mysqli_num_rows($result) > 0){
            
        $res='<div class="row">
        <div class="col s0 m6">
          <div class="card blue-grey darken-1 ">
            <div class="card-content black-text">
              <span class="card-title">Frutas de México</span>';
            
            
            
        $res.="<table class = \"highlight\"><thead><tr><th>nombre</th><th>unidades</th><th>cantidad</th><th>precio</th><th>pais</th></tr></thead><tbody>";
        while( $row = mysqli_fetch_assoc($result)){
            $res.="<tr>".
            "<td>" . $row["name"] . "</td>".
            "<td>" . $row["units"] . "</td>".
            "<td>" . $row["quantity"] . "</td>".
            "<td>" . $row["price"] . "</td>".
            "<td>" . $row["country"] . "</td>".
            "</tr>";
        }
        $res.='</tbody></table>
        </div>
          </div>
        </div>';
    }
        return $res;
    }

    function getFruitsUnits(){
        $conn = connectDb();
        $sql = "SELECT name, units, quantity, price, country FROM Fruit ORDER BY units";
        $result = mysqli_query($conn, $sql);
        if(mysqli_num_rows($result) > 0){
            
        $res='
        <div class="col s6 m6">
          <div class="card blue-grey darken-1 ">
            <div class="card-content black-text">
              <span class="card-title">Frutas de México</span>';
            
            
            
        $res.="<table class = \"highlight\"><thead><tr><th>nombre</th><th>unidades</th><th>cantidad</th><th>precio</th><th>pais</th></tr></thead><tbody>";
        while( $row = mysqli_fetch_assoc($result)){
            $res.="<tr>".
            "<td>" . $row["name"] . "</td>".
            "<td>" . $row["units"] . "</td>".
            "<td>" . $row["quantity"] . "</td>".
            "<td>" . $row["price"] . "</td>".
            "<td>" . $row["country"] . "</td>".
            "</tr>";
        }
        $res.='</tbody></table>
        </div>
          </div>
        </div>
      </div>';
    }
        return $res;
    }

    function getFruitsCards(){
        $db = connectDb();
    
    //Specification of the SQL query
    $query='SELECT * FROM Fruit';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
    
    $cards = '<div class="row">';
     // cycle to explode every line of the results
    $i=0;
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
    	$cards .= '
        <div class="col s'.(($i%4)*4).' m4">
          <div class="card pink lighten-3">
            <div class="card-content white-text">
              <span class="card-title"><strong>'.$fila["name"].'</strong></span>
              <p><strong>Unidades: </strong>'.$fila["units"].'</p>
              <p><strong>Cantidad: </strong>'.$fila["quantity"].'</p>
              <p><strong>Precio: </strong>'.$fila["price"].'</p>
              <p><strong>País: '.$fila["country"].'</strong></p>
            </div>
            <div class="card-action">
            </div>
          </div>
        </div>';
      $i += 1;
    }
    $cards .='
    <div class="col s'.(($i%4)*4).' m4">
          <div class="card pink lighten-3">
            <div class="card-content white-text centered">
              <a class="btn-floating btn-large waves-effect waves-light blue lighten-2"><i class="material-icons">add</i></a>
                </div>
            <div class="card-action">
            </div>
          </div>
        </div>
    </div>';
    // it releases the associated results
    mysqli_free_result($registros);
    
    closeDb($db);
    
    return $cards;
    }
?>