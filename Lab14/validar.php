<?php

session_start();

function _e($string) {
  return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
  //echo h
  tmlentities($string, ENT_QUOTES, 'UTF-8');
}


if(isset($_POST["nombre"]) && isset($_POST["apellido"]) && isset($_POST["mail"]) && isset($_POST["con_mail"]) && isset($_POST["contrasenia"]) && isset($_POST["con_contrasenia"])){


    if(empty($_POST["nombre"]) || empty($_POST["apellido"]) || empty($_POST["mail"]) || empty($_POST["con_mail"]) || empty($_POST["contrasenia"]) || empty($_POST["con_contrasenia"])){
        $m = "Los datos que ingresaste no están completos";
        include("_Nav.html");
        include("_Error.html");
        include("_Footer.html");
    }
    else if($_POST["mail"] != $_POST["con_mail"]){
        $m = "Los correos que ingresaste no coinciden";
        include("_Nav.html");
        include("_Error.html");
        include("_Footer.html");
    }
    else if($_POST["contrasenia"] != $_POST["con_contrasenia"]){
        $m = "Las contraseñas que ingresaste no coinciden";
        include("_Nav.html");
        include("_Error.html");
        include("_Footer.html");
    }else{
      $_SESSION["nombre"] = $_POST["nombre"];
      $_SESSION["apellido"] = $_POST["apellido"];
      $_SESSION["mail"] = $_POST["mail"];
      $_SESSION["contrasenia"] = $_POST["contrasenia"];

      $fn=_e($_SESSION["nombre"]);
      $ln=_e($_SESSION["apellido"]);
      $em=_e($_SESSION["mail"]);
      $ps=_e($_SESSION["contrasenia"]);

      $target_dir = "media/";
      $target_file = $target_dir . basename($_FILES["foto"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["foto"]["tmp_name"]);
            if($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
        // Check file size
        if ($_FILES["foto"]["size"] > 50000000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["foto"]["tmp_name"], $target_file)) {
                echo "The file ". basename( $_FILES["foto"]["name"]). " has been uploaded.";
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }

        $_SESSION["foto"] = $target_file;

      include("_Nav.html");
      include("_Valido.html");
      include("_Footer.html");
    }
}else{
    header("Location:index.php");
}


?>
