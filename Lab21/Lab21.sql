IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'creaMaterial' AND type = 'P')
                DROP PROCEDURE creaMaterial
            GO
            
            CREATE PROCEDURE creaMaterial
                @uclave NUMERIC(5,0),
                @udescripcion VARCHAR(50),
                @ucosto NUMERIC(8,2),
                @uimpuesto NUMERIC(6,2)
            AS
                INSERT INTO Materiales VALUES(@uclave, @udescripcion, @ucosto, @uimpuesto)
            GO

EXECUTE creaMaterial 5000,'Martillos Acme',250,15 

SELECT *
FROM Materiales

IF EXISTS(SELECT name FROM sysobjects 
          WHERE name = 'modificaMaterial' AND type = 'P')
		  DROP PROCEDURE modificaMaterial
	GO
		
		CREATE PROCEDURE modificaMaterial
			@uclave NUMERIC(5,0),
            @udescripcion VARCHAR(50),
            @ucosto NUMERIC(8,2),
            @uimpuesto NUMERIC(6,2)
		AS
			UPDATE Materiales SET Descripcion = @udescripcion,
								  Costo = @ucosto,
								  PorcentajeImpuesto = @uimpuesto
								  WHERE Clave = @uclave
	GO

EXECUTE modificaMaterial 5000,'piedras del infierno 2',666,16

SELECT *
FROM Materiales

IF EXISTS(SELECT name FROM sysobjects 
          WHERE name = 'eliminaMaterial' AND type = 'P')
		  DROP PROCEDURE eliminaMaterial
	GO
		
		CREATE PROCEDURE eliminaMaterial
			@uclave NUMERIC(5,0)
		AS
			DELETE FROM Materiales WHERE Clave = @uclave
	GO

EXECUTE eliminaMaterial 5000

SELECT *
FROM Materiales

IF EXISTS(SELECT name FROM sysobjects
		  WHERE name = 'creaProyecto')
		  DROP PROCEDURE creaProyecto
	GO

	CREATE PROCEDURE creaProyecto
					 @uNumero NUMERIC(5,0),
					 @uDenominacion VARCHAR(50)
	AS
		INSERT INTO Proyectos VALUES(@uNumero, @uDenominacion)

	GO

EXECUTE creaProyecto 666,'Highway to hell'

SELECT *
FROM Proyectos

IF EXISTS(SELECT name FROM sysobjects 
          WHERE name = 'modificaProyecto' AND type = 'P')
		  DROP PROCEDURE modificaProyecto
	GO
		
		CREATE PROCEDURE modificaProyecto
			@uNumero NUMERIC(5,0),
            @uDenominacion VARCHAR(50)
		AS
			UPDATE Proyectos SET Denominacion = @uDenominacion
								 WHERE Numero = @uNumero
	GO

EXECUTE modificaProyecto 666,'OZZY stage'

SELECT *
FROM Proyectos

IF EXISTS(SELECT name FROM sysobjects 
          WHERE name = 'eliminarProyecto' AND type = 'P')
		  DROP PROCEDURE eliminarProyecto
	GO

	CREATE PROCEDURE eliminarProyecto
		@unumero NUMERIC(5,0)
	AS
		DELETE FROM Proyectos WHERE Numero = @unumero
	GO

EXECUTE eliminarProyecto 666

SELECT *
FROM Proyectos

IF EXISTS(SELECT name FROM sysobjects 
          WHERE name = 'crearProveedor' AND type = 'P')
		  DROP PROCEDURE crearProveedor
	GO

		CREATE PROCEDURE crearProveedor
						 @uRFC VARCHAR(50),
						 @uRazonSocial VARCHAR(50)
		AS
			INSERT INTO Proveedores VALUES(@uRFC, @uRazonSocial)

EXECUTE crearProveedor 'DOGG666420','Snoop Dogg'

SELECT *
FROM Proveedores

IF EXISTS(SELECT name FROM sysobjects 
          WHERE name = 'modificarProveedor' AND type = 'P')
		  DROP PROCEDURE modificarProveedor
	GO

IF EXISTS(SELECT name FROM sysobjects 
          WHERE name = 'modificarProveedor' AND type = 'P')
		  DROP PROCEDURE modificarProveedor
	GO
	CREATE PROCEDURE modificarProveedor
					 @uRFC VARCHAR(50),
					 @uRazonSocial VARCHAR(50)
	AS
		UPDATE Proveedores SET RazonSocial = @uRazonSocial WHERE RFC = @uRFC

EXECUTE modificarProvedor DOGG666420,'wKhalifa'

SELECT *
FROM Proveedores

IF EXISTS(SELECT name FROM sysobjects 
          WHERE name = 'eliminarProveedor' AND type = 'P')
		  DROP PROCEDURE eliminarProveedor
	GO

	CREATE PROCEDURE eliminarProveedor
					 @uRFC VARCHAR(50)
			AS
				DELETE FROM Proveedores WHERE RFC = @uRFC
	GO

EXECUTE eliminarProveedor DOGG666420

SELECT *
FROM Entregan

SET DATEFORMAT DMY
IF EXISTS (SELECT name FROM sysobjects 
                       WHERE name = 'creaEntregan' AND type = 'P')
                DROP PROCEDURE creaEntregan
            GO
            
            CREATE PROCEDURE creaEntregan
                @uclave NUMERIC(5,0),
				@uRFC VARCHAR(50),
				@uNumero NUMERIC(5,0),
				@uFecha DATETIME,
				@uCantidad DECIMAL(5,2)
            AS
                INSERT INTO Entregan VALUES(@uclave, @uRFC, @uNumero, @uFecha, @uCantidad)
            GO

EXECUTE creaEntregan 1400,'HHHH800101',5010,'24-12-2017', 666.00 

SELECT *
FROM Entregan

IF EXISTS(SELECT name FROM sysobjects 
          WHERE name = 'modificaEntregan' AND type = 'P')
		  DROP PROCEDURE modificaEntegan
	GO
		
		CREATE PROCEDURE modificaEntregan
			@uclave NUMERIC(5,0),
			@uRFC VARCHAR(50),
			@uNumero NUMERIC(5,0),
			@uFecha DATETIME,
			@uCantidad DECIMAL(5,2)
		AS
			UPDATE Entregan SET Fecha = @uFecha,
								Cantidad = @uCantidad
							WHERE Clave = @uclave AND RFC = @uRFC AND Numero = @uNumero
	GO

EXECUTE modificaEntregan 1400,'HHHH800101',5010,'09-01-1998', 420.00 

SELECT *
FROM Entregan

IF EXISTS(SELECT name FROM sysobjects 
          WHERE name = 'eliminaEntregan' AND type = 'P')
		  DROP PROCEDURE eliminaEntregan
	GO
		
		CREATE PROCEDURE eliminaEntregan
			@uclave NUMERIC(5,0),
			@uRFC VARCHAR(50),
			@uNumero NUMERIC(5,0),
			@uFecha DATETIME,
			@uCantidad DECIMAL(5,2)
		AS
			DELETE FROM Entregan WHERE Clave = @uclave
									AND RFC = @uRFC
									AND Numero = @uNumero
									AND Fecha = @uFecha
									AND Cantidad = @uCantidad
	GO

EXECUTE eliminaEntregan 1400,'HHHH800101',5010,'09-01-1998', 420.00 

SELECT *
FROM Entregan

IF EXISTS (SELECT name FROM sysobjects 
                                       WHERE name = 'queryMaterial' AND type = 'P')
                                DROP PROCEDURE queryMaterial
                            GO
                            
                            CREATE PROCEDURE queryMaterial
                                @udescripcion VARCHAR(50),
                                @ucosto NUMERIC(8,2)
                            
                            AS
                                SELECT * FROM Materiales WHERE descripcion 
                                LIKE '%'+@udescripcion+'%' AND costo > @ucosto 
                            GO

EXECUTE queryMaterial 'var', 30
EXECUTE queryMaterial 'Lad',20