--Eliminar las tablas--
drop TABLE entregan 
drop TABLE materiales 
drop TABLE proyectos 
drop TABLE Proveedores 


--Crear las tablas--

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Materiales')
DROP TABLE Materiales

CREATE TABLE Materiales 
( 
  Clave numeric(5) not null, 
  Descripcion varchar(50), 
  Costo numeric (8,2) 
) 


IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proveedores')
DROP TABLE Proveedores

CREATE TABLE Proveedores 
( 
  RFC char(13) not null, 
  RazonSocial varchar(50) 
) 
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proyectos')
DROP TABLE Proyectos

CREATE TABLE Proyectos 
( 
  Numero numeric(5) not null, 
  Denominacion varchar(50) 
) 
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Entregan')
DROP TABLE Entregan

CREATE TABLE Entregan 
( 
  Clave numeric(5) not null, 
  RFC char(13) not null, 
  Numero numeric(5) not null, 
  Fecha DateTime not null, 
  Cantidad numeric (8,2) 
)

--Subir información--
BULK INSERT a1350836.a1350836.[Materiales]
	FROM 'e:\wwwroot\a1701448\materiales.csv'
	WITH
	(
		CODEPAGE='ACP',
		FIELDTERMINATOR=',',
		ROWTERMINATOR='\n'
	)

BULK INSERT a1350836.a1350836.[Proyectos]
	FROM 'e:\wwwroot\a1701448\proyectos.csv'
	WITH
	(
		CODEPAGE='ACP',
		FIELDTERMINATOR=',',
		ROWTERMINATOR='\n'
	)

BULK INSERT a1350836.a1350836.[Proveedores]
	FROM 'e:\wwwroot\a1701448\proveedores.csv'
	WITH
	(
		CODEPAGE='ACP',
		FIELDTERMINATOR=',',
		ROWTERMINATOR='\n'
	)

SET DATEFORMAT dmy

BULK INSERT a1350836.a1350836.[Entregan]
	FROM 'e:\wwwroot\a1701448\entregan.csv'
	WITH
	(
		CODEPAGE='ACP',
		FIELDTERMINATOR=',',
		ROWTERMINATOR='\n'
	)

--Ejercicio 2--

 INSERT INTO Materiales values(1000, 'xxx', 1000)
 
 SELECT * FROM Materiales 

 Delete from Materiales where Clave = 1000 and Costo = 1000 

 ALTER TABLE Materiales add constraint llaveMateriales PRIMARY KEY(Clave)

 INSERT INTO Materiales values(1000, 'xxx', 1000)

 sp_helpconstraint materiales 

 ALTER TABLE Proyectos add constraint llaveProyectos PRIMARY KEY(Numero)

 ALTER TABLE Proveedores add constraint llaveProveedores PRIMARY KEY(RFC)

 ALTER TABLE Entregan add constraint llaveEntregan PRIMARY KEY(Clave, RFC, Numero, Fecha)

 sp_helpconstraint entregan

 --Ejercicio 3--

 SELECT * FROM Materiales
 
 SELECT * FROM Proveedores

 SELECT * FROM Proyectos

 SELECT * FROM Entregan

INSERT INTO entregan values (0, 'xxx', 0, '1-jan-02', 0)

DELETE FROM Entregan WHERE Clave = 0

ALTER TABLE Entregan add constraint cfEntreganClave FOREIGN KEY(Clave) REFERENCES Materiales(Clave)

ALTER TABLE Entregan add constraint cfEntreganRFC FOREIGN KEY(RFC) REFERENCES Proveedores(RFC)

ALTER TABLE Entregan add constraint cfEntreganNumero FOREIGN KEY(Numero) REFERENCES Proyectos(Numero)

INSERT INTO Entregan values (0, 'xxx', 0, '1-jan-02', 0)

sp_helpconstraint Materiales

sp_helpconstraint Proveedores

sp_helpconstraint Proyectos

sp_helpconstraint Entregan

--Ejercicio 4--

INSERT INTO entregan values (1000, 'AAAA800101', 5000, GETDATE(), 0)

DELETE FROM Entregan WHERE Cantidad = 0

ALTER TABLE Entregan add constraint cantidad check(cantidad>0)

INSERT INTO entregan values (1000, 'AAAA800101', 5000, GETDATE(), 0)

sp_helpconstraint Entregan